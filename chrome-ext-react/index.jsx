import WelcomeController from './Controllers/WelcomeController.jsx'
import CheckoutController from './Controllers/CheckoutController.jsx'
import MakeCustomController from './Controllers/MakeCustomController.jsx'
import CustomViewController from './Controllers/CustomViewController.jsx'

var Route = new Map()

Route.set('/welcome.php', WelcomeController)
Route.set('/GetStandard.php', CheckoutController)
Route.set('/MakeCustom.php', MakeCustomController)
Route.set('/MakeCustom2.php', CustomViewController)

Route.get(document.location.pathname)()
