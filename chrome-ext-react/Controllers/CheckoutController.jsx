import $ from 'jquery'

export default function () {
  $('td[style*="orange"]').css({
    'background-color': '',
    'color': 'orange'
  })
  $("td[style*='red']").css({
    'background-color': '',
    'color': 'red'
  })

  $('#boxleft div').contents().filter(function () {
    return this.nodeType === 3
  }).remove()

  $('#seldescr, #boxleft div font[color="red"], #boxleft div font[color="orange"], #boxleft div > br').remove()
}
