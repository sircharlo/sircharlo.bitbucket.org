import $ from 'jquery'
import Territory from '../Repositories/Territory.jsx'
import parseDoor from '../Parsers/CustomViewParser.jsx'
import React from 'react'
import { Table, Checkbox } from 'react-bootstrap'
import { render } from 'react-dom'

export default () => {
  let doors = new Territory()

  $('tbody > tr').each((index, line) => doors.push(parseDoor(line)))

  let table = (
    <Table striped hover>
      <thead>
        <tr>
          <th></th>
          <th>Territory</th>
          <th>Name</th>
          <th>Address</th>
          <th>Phone #</th>
        </tr>
      </thead>
      <tbody>
        {
          doors.map((door, i) => (
            <tr key={i}>
              <td>
                <Checkbox name="TerrAddrRec[]" id="TerrAddrRec[]" value="{door.id}"></Checkbox>{i}
              </td>
              <td>{door.territory}</td>
              <td>{door.name}</td>
              <td>
                {door.getStreetAddress()}<br />
                {door.getRegionAddress()}
              </td>
              <td>{door.phone}</td>
            </tr>
          ))
        }
      </tbody>
    </Table>
  )

  let div = $('<div id="thetable"></div>').insertAfter('#mainform2 > p')[0]
  $('table').remove()

  render(table, div)
}
