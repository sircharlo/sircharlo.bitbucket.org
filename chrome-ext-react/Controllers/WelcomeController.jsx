import ToggleButton from '../Components/WelcomeToggleButton.jsx'
import $ from 'jquery'
import {render} from 'react-dom'
import React from 'react'

export default function () {
  render(<ToggleButton />, $('#announce > .panel-heading')[0])
}
