var path = require('path')
var exec = require('child_process').exec
var WebpackOnBuildPlugin = require('on-build-webpack')

const APP_PATH = path.resolve(__dirname)
const BUILD_PATH = path.resolve(__dirname, 'public/js')

var webpackConfig = {
  entry: APP_PATH + '/index.jsx',
  output: {
    path: BUILD_PATH,
    filename: 'app.js'
  },
  module: {
    loaders: [
      {
        'test': /\.jsx?/,
        'include': APP_PATH,
        'loader': 'babel'
      }
    ],
    plugins: [
      new WebpackOnBuildPlugin(stats => {
        exec('open "Google Chrome" http://reload.extensions')
      })
    ]
  }
}

module.exports = webpackConfig
