import React from 'react'
import $ from 'jquery'
import {Button} from 'react-bootstrap'

class ToggleButton extends React.Component {
  constructor () {
    super()
    this.state = {
      annsHidden: true
    }

    this.toggleAnns = this.toggleAnns.bind(this)
  }

  toggleAnns () {
    this.setState({annsHidden: !this.state.annsHidden})
  }

  render () {
    let anns = $('#announce table tr td:not(:contains("Olivier"))')
    if (this.state.annsHidden) anns.each((index, elem) => $(elem).hide())
    else anns.each((index, elem) => $(elem).show())

    const text = this.state.annsHidden ? 'Show' : 'Hide'
    return (
      <h3>
        Announcements
        <Button
          bsStyle="primary"
          className="pull-right"
          onClick={this.toggleAnns}>
          {text} irrelevant announcements
        </Button>
      </h3>
    )
  }
}

export default ToggleButton
