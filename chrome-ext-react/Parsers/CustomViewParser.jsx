import $ from 'jquery'
import Door from '../Repositories/Door.jsx'

export default function CustomViewParser (tr) {
  let admin = extractAdmin($(tr).find('td:eq(4)').text())

  return new Door({
    id: $(tr).find('input[type=checkbox]').val(),
    territory: $(tr).find('td:eq(1)').text(),
    name: $(tr).find('td:eq(2)').text(),
    street: $(tr).find('td:eq(3)').text().split(' -')[0],
    apt: $(tr).find('td:eq(3)').text().split(' -')[1] || '',
    city: admin.city,
    province: admin.province,
    postal_code: admin.postal_code,
    phone: $(tr).find('td:eq(6)').text()
  })
}

function extractAdmin (adminStr) {
  return {
    city: adminStr.split(', ')[0],
    province: adminStr.split(', ')[1].split(' ')[0],
    postal_code: adminStr.split(', ')[1].split(' ')[1] + ((' ' + adminStr.split(', ')[1].split(' ')[2]) || '')
  }
}
