export default class Door {
  constructor (door) {
    this.id = door.id
    this.territory = door.territory.trim()
    this.name = door.name.trim()
    this.address = {
      street: door.street.trim(),
      apt: door.apt.trim(),
      city: door.city.trim(),
      province: door.province.trim(),
      postal_code: door.postal_code.trim()
    }
    this.phone = this.parsePhone(door.phone.trim())
  }

  getFullAddress () {
    return `${this.getStreetAddress()}, ${this.getRegionAddress}`
  }

  getStreetAddress () {
    return `${this.address.street}${this.address.apt !== '' ? ' #'+this.address.apt : ''}`
  }

  getRegionAddress () {
    return `${this.address.city || '-'}, ${this.address.province || '-'}, ${this.address.postal_code || '-'}`
  }

  parsePhone (phone) {
    phone = phone.split('-').join('')

    return phone === '' ? '' : `${phone.substr(0,3)}-${phone.substr(3,3)}-${phone.substr(6)}`
  }
}
