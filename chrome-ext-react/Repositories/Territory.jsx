import _ from 'underscore'

export default class Territory {
  constructor () {
    this.doors = []
  }

  push (door) {
    this.doors.push(door)
    return this
  }

  map (func) {
    return this.doors.map(func)
  }
}
