var terName = $("#page b").html();
terName = terName.split(" - ").join("<br />")
$("#page b").html(terName);

$("#page span:first-child").prepend('<span id="printFieldContainers">Publisher: <input class="printField" id="publisherField" type="text" />   Due date: <input class="printField" id="dueDateField" type="text" /></span>');

$("#page span:eq(0)").css("width", "728px");

var monthNames = [
	"January", "February", "March",
	"April", "May", "June", "July",
	"August", "September", "October",
	"November", "December"
];

var date = new Date();
var day = date.getDate();
day = day.toString();
var dayLastDigit = day.charAt(day.length - 1);
var dayOrdinal;
if (dayLastDigit == 1 & day != 11) {
	dayOrdinal = "st";
} else if (dayLastDigit == 2 & day != 12) {
	dayOrdinal = "nd";
} else if (dayLastDigit == 3 & day != 13) {
	dayOrdinal = "rd";
} else {
	dayOrdinal = "th";
}

var monthIndex = date.getMonth();
var year = date.getFullYear();
if (monthIndex >= 9) {
	monthIndex = monthIndex - 9;
	year = year + 1;
} else {
	monthIndex = monthIndex + 3;
}

var fullDate = monthNames[monthIndex] + ' ' + day + dayOrdinal + ', ' + year;


var pubName = prompt("Please enter the publisher's name:");
$("#publisherField").val(pubName);

var datePrompt = prompt("Please enter the due date:", fullDate);
$("#dueDateField").val(datePrompt);

$("#content").width("100%");
$("#page").width("100%");
$("#mapdiv").width("728px");
$("#mapdiv").height("100%");
$("#mapdiv").css("margin-top", "1em");