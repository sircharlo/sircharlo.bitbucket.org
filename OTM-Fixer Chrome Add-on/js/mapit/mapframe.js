var locations = [ // temporary data, for testing only
    '6366b chester, montreal',
    '49 doucet , j3n1h9',
    '6262 biermans, montreal'
];


function GetURLParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return decodeURIComponent(sParameterName[1]);
        }
    }
}          
var addresses = GetURLParameter('addresses').trim();
alert(addresses);

addresses = addresses.replace(/<b>/g, ''); // and go on to format it a bit
addresses = addresses.replace(/<\/b>/g, '');
addresses = addresses.replace(/Address:/g, '');
addresses = addresses.replace(/Name:/g, '');
addresses = addresses.split('<p>');
for (i = 0; i < addresses.length; i++) {
    addresses[i] = addresses[i].substring(0, addresses[i].indexOf('</p></div>')); // i get rid of the system geotags here, remove later if necessary
    addresses[i] = addresses[i].split('<br>'); // now we have a multidimensional array
    addresses = addresses.filter(function(n){ return n != undefined });
    addresses[i][0] = addresses[i][0].trim();
    if(!(typeof addresses[i][1] === 'undefined')) {
    	addresses[i][1] = addresses[i][1].trim();
	}
} // result: an array with our placepoints

//alert(addresses);

function initialize() {
    var mapOptions = {
        zoom: 100,
        center: new google.maps.LatLng(-34.397, 150.644)
    };


var i = ""
    var map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);

    var markerBounds = new google.maps.LatLngBounds();
	var pinPoint;
    function codeAddress() {
        var address;
geocoder = new google.maps.Geocoder();
        for (i = 0; i < addresses.length; i++) {
            address = locations[i];//addresses[i][0];
            geocoder.geocode({
                'address': address
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    pinPoint = results[0].geometry.location;
                    var marker = new google.maps.Marker({
                        map: map,
                        position: pinPoint,
                        //title: addresses[i][1]
                    });
                    markerBounds.extend(pinPoint);
                    map.fitBounds(markerBounds)
                } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
            });
        }
            
    };
    codeAddress();
}

function loadScript() {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp' +
        '&signed_in=true&callback=initialize&key=AIzaSyB0tQuZoGrcZmK3Xd3UMjEBczc7jyibHj4';
    document.body.appendChild(script);
}

window.onload = loadScript;