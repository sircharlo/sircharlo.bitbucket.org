function hasWordMatch(a, b, case_sensitive) {
    if (case_sensitive !== true) {
        a = a.toLowerCase();
        b = b.toLowerCase();
    }
    var a_parts = a.split(' ');
    var b_parts = b.split(' ');
    var a_length = a_parts.length;
    var b_length = b_parts.length;
    var i_a, i_b;
    for (i_a = 0; i_a < a_length; i_a += 1) {
        for (i_b = 0; i_b < b_length; i_b += 1) {
            if (a_parts[i_a] === b_parts[i_b]) {
                if (a_parts[i_a].length > 1) {
                    return true;
                }
            }
        }
    }
    return false;
}