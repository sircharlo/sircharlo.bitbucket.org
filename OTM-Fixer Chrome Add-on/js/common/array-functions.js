Array.prototype.allIndexOf = function (searchElement)
{
    if (this === null) {
        return [-1];
    }
    var len = this.length,
        hasIndexOf = Array.prototype.indexOf, // you know, because of IE
        i = (hasIndexOf) ? this.indexOf(searchElement) : 0,
        n,
        indx = 0,
        result = [];
    if (len === 0 || i === -1) {
        return [-1];
    }
    if (hasIndexOf) {
        // Array.indexOf does exist
        for (n = 0; n <= len; n++) {
            i = this.indexOf(searchElement, indx);
            if (i !== -1) {
                indx = i + 1;
                result.push(i);
            } else {
                return result;
            }
        }
        return result;
    } else {
        // Array.indexOf doesn't exist
        for (n = 0; n <= len; n++) {
            if (this[n] === searchElement) {
                result.push(n);
            }
        }
        return (result.length > 0) ? result : [-1];
    }
};

Array.prototype.unique = function ()
{
    var r = [], q = []; // duplicate phone numbers

    o: for (var z = 0, n = this.length; z < n; z++) {
        for (var x = 0, y = r.length; x < y; x++) {
            if (r[x] == this[z] && this[z] != "") {
                q[q.length] = this[z];
                continue o;
            }
        }
        r[r.length] = this[z];
    }
    return r;
};

Array.prototype.remove = function (from, to) {
    var rest = this.slice((to || from) + 1 || this.length);
    this.length = from < 0 ? this.length + from : from;
    return this.push.apply(this, rest);
};