var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

var showallmyter = getUrlParameter('showallmyter');
console.log(showallmyter);

if (showallmyter != 1) {
    $("#mainform2 th:nth-child(10)").remove();
    $("#mainform2 th:nth-child(9)").remove();
    $("#mainform2 th:nth-child(8)").remove();
    $("#mainform2 th:nth-child(7)").remove();
    $("#mainform2 th:nth-child(4)").remove();
    $("#mainform2 th:nth-child(3)").remove();
    $("#mainform2 tr td:nth-child(10)").remove();
    $("#mainform2 tr td:nth-child(9)").remove();
    $("#mainform2 tr td:nth-child(8)").remove();
    $("#mainform2 tr td:nth-child(7)").remove();
    $("#mainform2 tr td:nth-child(4)").remove();
    $("#mainform2 tr td:nth-child(3)").remove();
    $("#mainform2 th:gt(0):lt(3)").css('text-align','center');
}
