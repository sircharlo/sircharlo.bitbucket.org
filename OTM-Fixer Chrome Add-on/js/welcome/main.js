$('#announce table tr td:not(:contains("Olivier"))').each(function(index)
{
    $(this).hide();
});
$('#announce table').append('<input class="hiddenCount btn btn-sm btn-primary" style="margin-top:20px; width: 210px;" value="Toggle irrelevant announcements">');
$('.hiddenCount').click(function(index)
{
    $('#announce table tr td:not(:contains("Olivier"))').toggle();
	$('.hiddenCount').show();
});