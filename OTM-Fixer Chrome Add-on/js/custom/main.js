var s = [];
var phones = [];
var doubles = [];
var customPhones = [];
var dupes = 0;
var countDupes = 0;
var dupeColors = [];
$("#content").prepend("<div id='promptArea'></div>");
$("#promptArea").width('100%');
$("#promptArea").css("background-color", "white");
$("#promptArea").append("<span class='sexyline'></span>");
// We're at the Custom edit page..
var tr = document.getElementsByTagName('tr') // Get all table rows..
for (var y = 0; y < tr.length; y++) { // ... and loop through them
    var textContent = tr[y].firstChild.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.textContent
    var tempPhone = "";
    tempPhone = textContent.replace(/\D+/g, ''); // sanitize phone numbers
    if (isNumber(tempPhone) && (tempPhone.length > 6)) { // and if it's really a phone number..
        customPhones.push(tempPhone); // .. put it in an array
    }
};
unique = customPhones.unique(); // grab all unique entries from array and dump in new array;
unique = unique.sort(); // sort the array;
phones = customPhones.sort(); // and
var g = 0;
for (var f = 0; f < unique.length; f++) { // more array sanitization crap
    if (unique[f] != customPhones[f]) {
        doubles[g] = customPhones[f];
        customPhones.remove(f);
        g++;
    }
}
for (var h = 0; h < doubles.length; h++) {
    randomColor = get_random_color();
    for (var y = 0; y < tr.length; y++) { // ... loop through rows again
        var textContent = tr[y].firstChild.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.textContent
        var textContentElement = tr[y].firstChild.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling
        var tempPhone = "";
        tempPhone = textContent.replace(/\D+/g, ''); // sanitize phone numbers
        // If the phone number is a dupe, color that row accordingly
        if ((tempPhone == doubles[h])) { // Dupe!
            textContentElement.parentNode.setAttribute('style', 'background-color: ' + randomColor);
            textContentElement.style.background = "rgba(0,0,0,0.15)";
            dupeColors.push(randomColor);
        };
    }
}
var names = new Array;
var streetNumber = new Array;
for (var y = 0; y < tr.length; y++) { // ... and loop through them
    names.push(tr[y].firstChild.nextElementSibling.nextElementSibling.textContent)
    streetNumber.push(tr[y].firstChild.nextElementSibling.nextElementSibling.nextElementSibling.textContent)
    streetNumber[y] = streetNumber[y].replace(/(\d*)(\D.+)/gim, '$1'); // only keep first numbers of address
    names[y] = names[y].replace(/[\.,-\/#!$%\^&\*;:{}=\-_`~()\d]/g, "")
    tr[y].firstChild.nextElementSibling.nextElementSibling.nextElementSibling
};
for (var p = 0; p < names.length; p++) {
    for (var q = 0; q < names.length; q++) {
        if (p != q && hasWordMatch(names[p], names[q]) && streetNumber[p] == streetNumber[q] && names[q] != "DUPED") {
            names[q] = "DUPED"
            if (!tr[p].style.backgroundColor) {
                randomColor = get_random_color();
                tr[p].style.backgroundColor = randomColor;
                tr[q].style.backgroundColor = randomColor;
                dupeColors.push(randomColor);
            } else {
                tr[q].style.backgroundColor = tr[p].style.backgroundColor;
            }
            tr[p].firstChild.nextElementSibling.nextElementSibling.style.background = "rgba(0,0,0,0.15)";
            tr[p].firstChild.nextElementSibling.nextElementSibling.nextElementSibling.style.background = "rgba(0,0,0,0.15)";
            tr[q].firstChild.nextElementSibling.nextElementSibling.style.background = "rgba(0,0,0,0.15)";
            tr[q].firstChild.nextElementSibling.nextElementSibling.nextElementSibling.style.background = "rgba(0,0,0,0.15)";
            dupes = 1;
        }
    }
};
if (doubles.length > 0 || dupes == 1) {
    document.getElementById('rowclick1').style.borderSpacing = "0";
    $("#promptArea").prepend("<p><input id='hideDoubles' type='checkbox' /> Hide non-double rows</p>");
    $("#promptArea").on("click change", "#hideDoubles", doHideDoubles);
    var tableId = '#rowclick1';
    var rowArr = getRows(tableId);
    var keySort = autoSortKeys(rowArr);
    var numSort = numSortKeys(rowArr)
    var coloredSort = 1;

    function doHideDoubles() {
        if ($("#hideDoubles").prop("checked") == true) {
            for (var y = 0; y < tr.length; y++) { // ... loop through rows again
                //if (tr[y].className != "head" && tr[y].style.display === "table-row") {
                if (tr[y].className != "head" && tr[y].style.backgroundColor == "") {
                    tr[y].style.display = "none";
                }
            }
            coloredSort = 1;
            sortTable(tableId, rowArr, keySort);
        } else {
            for (var y = 0; y < tr.length; y++) { // ... loop through rows again
                tr[y].style.display = "table-row";
                var x = document.getElementsByClassName("alt");
                var i;
                for (i = 0; i < x.length; i++) {
                    x[i].classList.remove('alt');
                }
            }
            coloredSort = 0;
            sortTable(tableId, rowArr, numSort);
        }
    };

    function sortTable(tableId, rows, keys) {
        $(tableId).empty();
        $.each(keys, function(indexK, valueK) {
            $.each(rows, function(indexR, valueR) {
                if (coloredSort == 0) {
                    if (valueR[2] === valueK) {
                        $(tableId).append(valueR[1]);
                    }
                } else if (valueR[0] === valueK) {
                    $(tableId).append(valueR[1]);
                }
            });
        });
        $(tableId + " tr.head").prependTo(tableId);
    }

    function rgbToInt(color) {
        var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(color);
        var red = parseInt(digits[2]);
        var green = parseInt(digits[3]);
        var blue = parseInt(digits[4]);
        return parseInt((blue | (green << 8) | (red << 16)), 10);
    };

    function getRows(tableId) {
        var arr = new Array();
        $(tableId + ' tr').each(function() {
            var bg = $(this).css('backgroundColor');
            var key = rgbToInt(bg);
            var origNum = $.trim($(this).children().first().text());
            arr.push([key, $(this), origNum]); // [bg-color, <tr />, original row number]
        });
        return arr;
    }

    function autoSortKeys(arr) {
        var keys = new Array();
        $.each(arr, function(index, value) {
            keys.push(value[0]);
        });
        var uniqueKeys = new Array();
        $.each(keys, function(index, value) {
            if ($.inArray(value, uniqueKeys) === -1)
                uniqueKeys.push(value);
        });
        delete keys; // garbage collect
        return uniqueKeys.sort().reverse();
    }

    function numSortKeys(arr) {
        var keys = new Array();
        $.each(arr, function(index, value) {
            keys.push(value[2]);
        });
        return keys; //.sort().reverse();
    }
    $("#hideDoubles").prop("checked", true);
    doHideDoubles();
    var uniqueKeys = new Array();
    $.each(dupeColors, function(index, value) {
        if ($.inArray(value, uniqueKeys) === -1)
            uniqueKeys.push(value);
    });
    var dupeSetCount = uniqueKeys.length;
    plural = (dupeSetCount > 1) ? "s" : "";
    $("#promptArea").prepend("<p>" + dupeSetCount + " set" + plural + " of potential doubles detected.</p>");
} else {
    $("#promptArea").prepend("<p>This territory doesn't seem to contain any doubles, according to the automated checks.</p>");
}
$(".sexyline").css({
    "display": "block",
    "border": "none",
    "color": "white",
    "height": "1px",
    "background": "black",
    "background": "-webkit-gradient(radial, 50% 50%, 0, 50% 50%, 900, from(#000), to(#fff))",
    "margin-bottom": "1em"
});
$("a[title*='Whitepages.com name']").each(function(index) {
    $(this).prop("href", "http://www.whitepages.com/name/" + this.text.replace(/ /g, '-').replace(/([\.\,]+)/g, '') + "/Montreal-QC");
});
$("a[title*='Whitepages.com Address']").each(function(index) {
    var address = []
    address = this.text.split('\xA0');
    var cleanAddress = address[0] + "+" + address[1];
    $(this).prop("href", "http://www.whitepages.com/search/FindNearby?utf8=%E2%9C%93&street=" + cleanAddress.replace(/ /g, '+').replace(/([\.\,]+)/g, '') + "&where=Montreal+QC");
});