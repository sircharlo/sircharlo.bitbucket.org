if (!('contains' in String.prototype)) {
	String.prototype.contains = function(str, startIndex) {
		return ''.indexOf.call(this, str, startIndex) !== -1;
	};

}

var streetsArray, similarStreets;
var form = document.getElementById("mainform2"); // This is our Edit form


streetsArray = [];
similarStreets = [];


$('[name*="Zip"]').val(function(i, val) {
	return val.toUpperCase();
});

String.prototype.toTitleCase = function() {
	var smallWords = /^(a|an|and|as|at|but|by|en|de|des|du|for|if|in|la|le|nor|of|on|or|per|the|to|vs?\.?|via)$/i;
	return this.replace(/[A-Za-z0-9\u00C0-\u00FF]+[^\s-]*/g, function(match, index, title) {
		if (index > 0 && index + match.length !== title.length &&
			match.search(smallWords) > -1 && title.charAt(index - 2) !== ":" &&
			(title.charAt(index + match.length) !== '-' || title.charAt(index - 1) === '-') &&
			title.charAt(index - 1).search(/[^\s-]/) < 0) {
			return match.toLowerCase();
		}
		if (match.substr(1).search(/[A-Z]|\../) > -1) {
			return match;
		}
		return match.charAt(0).toUpperCase() + match.substr(1);
	});
};


promptArea = document.getElementById("promptArea");
if (promptArea == null) {
	theKid = document.createElement("div");
	theKid.innerHTML = '';
	theKid.setAttribute("id", "promptArea");
	$(form.firstChild).after(theKid);
}

$("#mainform2 ol:first").hide();
$("#promptArea").width('100%');
$("#promptArea").css("background-color", "white");
$("#promptArea").css("font-size", "small");
$("#promptArea").append('<p><input class="btn btn-sm btn-default" type="submit" name="next" id="next" value="Next" onclick="document.pressed=this.value"> <input class="btn btn-sm btn-default" type="submit" name="cancel" id="cancel" value="Cancel"> | <input class="btn btn-sm btn-default" type="button" name="workedDate" id="workedDate" value="Set worked date" /> <input class="datepicker hasDatepicker" type="textbox" id="WrkdDate" name="WrkdDate" value="" size="12"> | <input class="btn btn-sm btn-default" id="srButton" type="button" value="Show search/replace" /> <input id="daToggle" class="btn btn-sm btn-default" type="button" style="display:none" value="Hide potential duplicate street names" /> <input id="psToggle" class="btn btn-sm btn-default" type="button" style="display:none" value="Hide streets with no prefixes" /></p><p><span id="sr" style="display:none"><span style="width: 85px;display: inline-block;">Search text:</span><input id="searchText" type="text"><br/><span style="width: 85px;display: inline-block; margin-top: 5px;">Replace text:</span><input id="replaceText" type="text"><br/><input id="srGo" class="btn btn-sm btn-default" type="button" style="margin-left: 85px; margin-top: 5px;" value="Replace" /></span></p>');

$("[name='next']:gt(0)").hide();
$("[name='cancel']:gt(0)").hide();
$("[name='WrkdDate']:eq(1)").hide();
var d = new Date();
var month = d.getMonth() + 1;
var day = d.getDate();
var output = (('' + month).length < 2 ? '0' : '') + month + '/' +
	(('' + day).length < 2 ? '0' : '') + day + '/' + d.getFullYear();
$("[name='WrkdDate']").val(output);
$("[name='myCheck']:eq(1)").hide();

$("#mainform2 div:eq(1)").hide();
$(".btn-sm").css("line-height", "1.25")



$("#promptArea").append("<div id='streetDupes'></div>");
$("#promptArea").append("<div id='prefixlessStreets'></div>");


$("#promptArea").on("click", "#srButton", function() {
	$("#sr").toggle();
	if ($('#sr').is(":visible")) {
		$("#srButton").prop("value", "Hide search/replace");
	} else {
		$("#srButton").prop("value", "Show search/replace");
	}
});

$("#promptArea").on("click", "#workedDate", function() {
	$("input[name*='WorkedDate']").val($("#WrkdDate").val());
});


function streetDupeFinder() {
	streetsArray = [];
	streetsArrayUnique = [];
    prefixlessStreets = [];
	similarStreets = [];
	$("input[name^='Street']").each(function(index) {
		streetsArray.push(this.value);
		var dist = [
			[]
		];
		streetsArrayUnique = streetsArray.unique(); // grab all unique entries from array and dump in new array;
		streetsArrayUnique.sort();
		for (var p = 0; p < streetsArrayUnique.length; p++) { // Loop through array elements one by one
			dist[p] = [];
			for (var q = 0; q < streetsArrayUnique.length; q++) {
				distArray = levenshteinenator(streetsArrayUnique[p], streetsArrayUnique[q]);
				dist[p].push(distArray[distArray.length - 1][distArray[distArray.length - 1].length - 1]);
			}
		}
		for (var p = 0; p < streetsArrayUnique.length; p++) { // Loop through array elements one by one
			for (var q = 0; q < streetsArrayUnique.length; q++) {
				if (streetsArrayUnique[p].length > 8 && streetsArrayUnique[q].length > 8) {
					if (dist[p][q] < 3 && dist[p][q] > 0) { // <-- Here's the Levenshtein factor
						similarStreets.push(streetsArrayUnique[p] + String.fromCharCode(160) + "and" + String.fromCharCode(160) + streetsArrayUnique[q]) // + " ("+dist[p][q]+")") <-- for debug, show levenshtein factor
						dist[q][p] = 1000
					}
				} else {
					if (streetsArrayUnique[p].length = streetsArrayUnique[q].length) {
						if (dist[p][q] < 3 && dist[p][q] > 0) {

							if (streetsArrayUnique[p].replace(/\D/g, '') == streetsArrayUnique[q].replace(/\D/g, '')) { // same number for av
								similarStreets.push(streetsArrayUnique[p] + String.fromCharCode(160) + "and" + String.fromCharCode(160) + streetsArrayUnique[q]) // + " ("+dist[p][q]+")") <-- for debug, show levenshtein factor
								dist[q][p] = 1000
							}
						}
					} else {
						similarStreets.push(streetsArrayUnique[p] + String.fromCharCode(160) + "and" + String.fromCharCode(160) + streetsArrayUnique[q]) // + " ("+dist[p][q]+")") <-- for debug, show levenshtein factor
						dist[q][p] = 1000
					}

				}
			}
		}
		if (similarStreets.length > 0) {
			$("#daToggle").show();
			$('#streetDupes').html("<p><span style='font-weight:bold'>Potential duplicate street names</span><br/>Here is an automatically generated list of potentially similar street names. If required, please clean up these street names, as well as prefixes and suffixes, to have uniform street names throughout the territory. This is to avoid sorting issues.<br/>Keep in mind that this is still a work in progress, so the results may, at times, be slightly odd.</p><ul><li>" + similarStreets.unique().join('</li><li>') + "</li</ul>");
		} else {
			$("#daToggle").hide();
			$('#streetDupes').html("");
		}
        var prefix = this.value.substring(0,5).slice(0, this.value.indexOf(" "));
        if (prefix != "Rue" && (prefix.indexOf(".")) == -1) {
            prefixlessStreets.push(this.value);
        }
        
        if (prefixlessStreets.length > 0) {
			$("#psToggle").show();
			$('#prefixlessStreets').html("<p><span style='font-weight:bold'>Streets with no prefix</span><br/>Here is a list of streets with no prefixes. If required, please clean up these street names, as well as prefixes and suffixes, to have uniform street names throughout the territory. This is to avoid sorting issues.</p><ul><li>" + prefixlessStreets.unique().join('</li><li>') + "</li</ul>");
		} else {
			$("#psToggle").hide();
			$('#prefixlessStreets').html("");
		}
	})
}


$("#promptArea").on("click", "#daToggle", function() {
	$('#streetDupes').toggle();
	if ($('#streetDupes').is(":visible")) {
		$("#daToggle").prop("value", "Hide potential duplicate street names");
	} else {
		$("#daToggle").prop("value", "Show potential duplicate street names");
	}
});

$("#promptArea").on("click", "#psToggle", function() {
	$('#prefixlessStreets').toggle();
	if ($('#prefixlessStreets').is(":visible")) {
		$("#psToggle").prop("value", "Hide streets with no prefixes");
	} else {
		$("#psToggle").prop("value", "Show streets with no prefixes");
	}
});

streetDupeFinder();
$("input[name^='Street']").on("change keyup paste", streetDupeFinder);

$(".row div:nth-child(9)").hide()
$(".row div:nth-child(5)").hide()
$(".row div:nth-child(4)").hide()
$(".row div:nth-child(3)").hide()
$("input[name*='Dir']").val("").hide();
$(".row").css("width", "1030px");
$(".row div:not(.form-group):nth-child(1)").css("width", "40px");
$(".row div:not(.form-group):nth-child(2)").css("width", "150px");
$(".row div:not(.form-group):nth-child(6)").css("width", "200px");
$(".row div:not(.form-group):nth-child(7)").css("width", "460px");
$(".row div input[type='text']").css("width", "100%");
$(".row div:not(.form-group):nth-child(7) input[type='text']:nth-child(1)").css("width", "100px");
$(".row div:not(.form-group):nth-child(7) input[type='text']:nth-child(3)").css("width", "250px");
$(".row div:not(.form-group):nth-child(7) input[type='text']:nth-child(4)").css("width", "100px");
$(".row input[name*='Name'],[name*='Street']").each(function() {
	var tempVal = $(this).val();
	tempVal = tempVal.toTitleCase();
	$(this).val(tempVal)
});

var newValue = ""; // initialize var for new value
var field = $("input[name*='Name'],[name*='Street'").each(function() { // iterate over every x
	newValue = ""; // empty previous final value
	$.each(this.attributes, function() { // loop over attributes
		if (this.specified) { // I don't really know why this is here
			if (this.name == "value") { 
				newValue += this.value; // attr value
			} else if (this.name != "onchange" && this.name != "class" && this.name != "type" && this.name != "name" && this.name != "size" && this.name != "maxlength" && this.name != "style" && this.name != "width" && this.name.indexOf("updgeo") === -1) {
				if (this.name != "") {newValue += "'" + this.name;} // attr name is actually a value
				if (this.value != "") {newValue += "'" + this.value;}
			}
		}
		return newValue;
	});
	if (newValue != $(this).val()) { // is there something to correct?
		$(this).val(newValue.replace(/\'+$/, "").toTitleCase()); // then correct it!
		$(this).css("color", "red")
	}
});

$("#promptArea").on("click", "#srGo", function() {
	searchValue = $("#searchText").val();
	replaceValue = $("#replaceText").val();
	if (searchValue != "") {
		searchValueRegEx = new RegExp(searchValue, "gim");
		reallyReplaceYesNo = confirm("Are you sure you want to replace all occurences of \"" + searchValue + "\" with \"" + replaceValue + "\"?");
	} else {
		alert("No search text was entered.");
	}
	if (reallyReplaceYesNo) {
        $("input[name*='Name'],[name*='Street'],[name*='Notes']").each(function() {
            var result = $(this).val().match(searchValueRegEx);
            if (result){
                var tempRep = $(this).val().replace(searchValueRegEx, replaceValue).toTitleCase();
                 $(this).val(tempRep);
            } 
        });
		streetDupeFinder();
	}
});