$("input[name*='Latitude']").each(function(index) {
	var latitude,
	    currentRow;
	latitude = $(this).val();
	currentRow = $(this).parent().parent();
	if (latitude == "" || Number(latitude) == 0) {
		africanDoor(currentRow);
	}
	//    $(this).prop("href", "http://www.whitepages.com/search/FindNearby?utf8=%E2%9C%93&street=" + cleanAddress.replace(/ /g, '+').replace(/([\.\,]+)/g, '') + "&where=Montreal+QC");
});

function africanDoor(affectedRow) {
	$("tr").hide();
	affectedRow.addClass("african");
	affectedRow.nextUntil($("[bgcolor='#97CFF0']"), "tr").addClass("african");
	affectedRow.prevUntil($("[bgcolor='#97CFF0']"), "tr").addClass("african");
	affectedRow.prevAll("[bgcolor='#97CFF0']:first").addClass("african");
	$("tr.african").show();
}
$("#mainform2 h2").html($("#mainform2 h2").html().replace('Make necessary changes and press Save to continue:', "Please correct the geotags for these entries:"));