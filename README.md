# Welcome! #

Here you'll find some quick hacks for Online Territory Manager.

To use, install the **OTM Fixer** Chrome extension.

---

### To do ###

* Batch field filler? (ie: City or Province)

### Done ###

* Implement page break on print for second (third, etc.) table
* Implement basic double checking (with phone number)
* Correct capitalization for articles (de, du, le, la, etc.)
* Correct capitalization for "d'Xxx" (Not "D'Xxx")
* Get street name from first field before processing (to override bug where everything after first apostrophe is stripped)
* Trim fields (remove trailing and leading whitespace)
* If last character of street is single letter, correct "N", "S", "E" and "O" to proper form
* Add "apt." before apartment number on printout
* Fix ProperCase function (presently, I think it corrects "côte" to "CôTe")
* Don't show "shaded entries" on map printout? --> no longer necessary, "Eng" and "Fr" doors deleted by order of elders
* Fix capitalization for articles preceded with "-" (Ex: "Côte-des-Neiges")
* Find and replace function
* Extend doubles-checking (with name and address)
